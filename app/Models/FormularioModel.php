<?php

namespace App\Models;

use CodeIgniter\Model;

class FormularioModel extends Model {

    protected $table = 'pau';
    protected $allowedFields = ['nif', 'apellido1', 'apellido2', 'nombre', 'email', 'ciclo', 'tipo_tasa', 'deleted_at'];

}
