<?php

namespace App\Models;

use CodeIgniter\Model;

class SolicitudesModel extends Model
{
    protected $table = 'pau';
    protected $allowedFields = ['id', 'nif', 'apellido1', 'apellido2', 'nombre', 'email', 'ciclo', 'tipo_tasa', 'deleted_at',];
    protected $validationRules = [
        'nif' => 'required|alpha_numeric_space|min_lenght[8]',
        'email' => 'required|min_lenght[5]|valid_email',
        'ciclo' => 'required|alpha_numeric_space',
        'tipo_tasa' => 'required|is_natural'
    ];
}
