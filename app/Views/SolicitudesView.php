
<div class="container">
    <table class="table caption-top" style="margin: 0 auto !important">

        <script>
    
$(document).ready(function() {
   $('#table_id').dataTable( {
       "language": {
           "info": "",
           "lengthMenu": "Mostrar _MENU_ por página",
           "paginate": {
               "next": "Siguiente",
               "previous": "Anterior"
           },
           "search": "Busca un alumno:",
       }
   } );
} );
</script>

<a class="btn btn-light btn-outline-info" href="<?= site_url('FormularioController/insertar') ?>"> Añadir </a>


        <caption>
        </caption>

        <thead>
            <tr>
                <th> id </th>
                <th> NIF </th>
                <th> Apellido1</th>
                <th> Apellido2</th>
                <th> Nombre</th>
                <th> Email </th>
                <th> Ciclo </th>
                <th> Matricula </th>
                <th>
                    <a class="btn btn-light btn-outline-danger" href="<?= site_url('SolicitantesController/borrar') ?>">Borrar</a>
                </th>
            </tr>
        </thead>

        <tbody>
            <?php
            foreach ($solicitantes as $solicitante) : ?>

                <tr class=" <?= $interval->format('%y') > 30 ? 'text-danger' : 'text' ?> ">
                    <td> <?= $solicitante["id"] ?> </td>
                    <td> <?= $solicitante["nif"] ?> </td>
                    <td> <?= $solicitante["apellido1"] ?> </td>
                    <td> <?= $solicitante["apellido2"] ?> </td>
                    <td> <?= $solicitante["nombre"] ?> </td>
                    <td> <?= $solicitante["email"] ?> </td>
                    <td> <?= $solicitante["ciclo"] ?> </td>
                    <td> <?= $solicitante["matricula"] ?> </td>
                    
                </tr>
            <?php
            endforeach; ?>
        </tbody>

    </table>
</div>
</body>