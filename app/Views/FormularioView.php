<html lang="es">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

        <title>FORMULARIO</title>
    </head>

    <style>

        body {

            
        }

        input:hover {

            background-color: paleturquoise;
            transform: scale(1.2);

        }

        div {

            margin: auto;
        }



    </style>
    
<?php if (!empty(\Config\Services::validation()->getErrors())):?>
<div class="alert alert-danger" role="alert">
    <?= \Config\Services::validation()->listErrors(); ?>
</div>
<?php endif; ?>
    
    <body>
        <br>
        <h2 style="text-align: center;">DATOS DEL SOLICITANTE</h2>
        <div class="container" id="alineacion">
            <form method="post" action="<?= base_url("index.php/SolicitudesController/insertar") ?>">
                <div class="form-group" style="width:450px;">
                    <i class="fas fa-id-card-alt"></i>&nbsp;<label name="nif">NIF</label>
                    <abbr title="Introduce 8 números"><input type="text" class="form-control" name="nia" id="nia" placeholder="Insertar NIA" maxlength="8" ></abbr>
                </div> <br>
                <div class="form-group" style="width:450px;">
                    <i class="fa fa-user bigicon"></i>&nbsp;<label name="apellido1">Primer Apellido</label>
                    <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Insertar Nombre">
                </div> <br>
                <div class="form-group" style="width:450px;">
                    <i class="fa fa-user bigicon"></i>&nbsp;<label name="apellido2">Segundo Apellido</label>
                    <input type="text" class="form-control" id="apellido1" name="apellido1" placeholder="Insertar Primer Apellido">
                </div> <br>
                <div class="form-group" style="width:450px;">
                    <i class="fa fa-user bigicon"></i>&nbsp;<label name="nombre">Nombre</label>
                    <input type="text" class="form-control" id="apellido2" name="apellido2" placeholder="Insertar Segundo Apellido">
                </div> <br>
                <div class="form-group" style="width:450px;">
                    <i class="fas fa-envelope-square"></i>&nbsp;<label name="email">Dirección de mail</label>
                    <input type="text" class="form-control" id="email" name="email" placeholder="Insertar tu email" >
                    
                </div> <br>
                <div class="form-group" style="width:450px;">
                    <i class="far fa-id-card"></i>&nbsp;<label name="ciclo">Ciclo</label>
                    <input type="text" class="form-control" id="nif" name="ciclo" placeholder="Insertar Ciclo" maxlength="9"></abbr>
                </div> <br>
                <div class="form-group" style="width:450px;">
                    <i class="far fa-id-card"></i>&nbsp;<label name="matricula">Matricula</label>
                    <input type="text" class="form-control" id="nif" name="matricula" placeholder="Insertar Matricula" maxlength="9"></abbr>
                </div> <br>
                
                 <!-- form_dropdown -->
                <br><div class="form-group" style="width:450px;">
                    <?= form_fieldset('Selecciona tu género') ?>
                    <?= form_label('Ciclo', 'ciclo', ['class' => 'control-label']) ?>
                    <?= form_dropdown('ciclo', ['af' => 'CFGS Administración y Finanzas', 'ci' => 'CFGS Comercio Internacional', 'ga' => 'CFGM Gestión Administrativa', 'hb' 
                        => 'CFGS Higiene Bucodental', 'pd' => 'CFGS Prótesis Dentales', 'smr' => 'CFGM Sistemas Microinformáticos y Redes', 'fp' => 'CFGM Farmacia y Parafarmacia',
                        'asir' => 'CFGS Administración de Sistemas Informáticos en Redes', 'daw' => 'CFGS Desarrollo de Aplicaciones Web', 'gvec' => 'CFGS Gestión de Ventas y Espacios Comerciales',
                        'ad' => 'CFGS Asistencia a la Dirección', 'ac' => 'CFGM Actividades Comerciales', 'p' => 'Prefiero no decirlo', 'opa' => 'CFGS Ortopŕotesis y Productos de Apoyo', 
                        'das' => 'CFGS Documentación y Administración Sanitarias', 'idmn' => 'CFGS Imagen para el Diagnóstico y Medicina Nuclear', 'lcb' => 'CFGS Laboratorio Clínico y Biomédico', 
                        'rd' => 'CFGS Radioterapia y Dosimetría'], 'ci', ['class' => 'custom-select', 'id' => 'ciclo']) ?>
                    <?= form_fieldset_close() ?>
                </div>
                
         
                <div class="custom-control custom-radio custom-control-inline" style="width:450px;">
                   <?=form_fieldset('Elige el tipo de tasa')?>
                    
                    <?= form_label('Ordinaria','Tipo_Tasa',['class'=>'control-label'])?>
                    <?= form_radio('tipo_tasa', 'Ordinaria', FALSE, ['class'=>'custom-radio', 'id'=>'Ordinaria'])?>

                    <?= form_label('Semigratuita','Tipo_Tasa',['class'=>'control-label'])?>
                    <?= form_radio('tipo_tasa', 'Semigratuita', FALSE, ['class'=>'custom-radio', 'id'=>'Semigratuita'])?>
                    
                    <?= form_label('Gratuita','Tipo_Tasa',['class'=>'control-label'])?>
                    <?= form_radio('tipo_tasa', 'Gratuita', FALSE, ['class'=>'custom-radio', 'id'=>'Gratuita'])?>
                 
                    <?=form_fieldset_close()?>
                </div>
                         
                          
                          
                <button type="submit" class="btn btn-secondary" style="margin-left:46%;"><i class="fas fa-upload"></i>&nbsp; Enviar</button>
            </form>
        </div>
    </body>
</html>


