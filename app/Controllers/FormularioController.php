<?php

namespace App\Controllers;

use App\Models\SolicitudesModel;
use App\Views\SolicitudesView;

  class FormularioController extends BaseController{
      public function index(){
          helper('form');
          $solicitantes = new FormularioController();
          echo view('FormularioView');
      }
      
      
      
      public function insertar(){
          helper('form');
          $data['validation'] = \Config\Services::validation();
          $model = new AlumnesModel();
        if ($this->validate($model->getValidationRules())){
          $alumno = [
            "NIF"=> $this->request->getPost('nif'),
            "Apellido1"=> $this->request->getPost('apellido1'),
            "Apellido2"=> $this->request->getPost('apellido2'),
            "Nombre" => $this->request->getPost('nombre'),
            "Email"=> $this->request->getPost('email'),
            "Ciclo" => $this->request->getPost('ciclo'),
            "Tasa" => $this->request->getPost('tipo_tasa'),
           
                  
          ];
           $model->insert($alumno);
          var_dump($alumno);
          echo "Acabo de insertar al alumno, ve a la tabla y compruebalo.";
        }
        else {
              
            echo view('FormularioView',$data);
        }

         
      }
  }
