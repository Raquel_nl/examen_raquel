<?php

namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\SolicitudesModel;
use App\Views\SolicitudesView;


class AlumnosController extends BaseController
{
    public function index()
    {
        helper('edad');
        
        $solicitantes = new SolicitudesModel();
        
        $data['solicitantes'] = $solicitantes->orderby('id','DESC')->findAll();
        
        return view('SolicitudesView', $data);
        
    }
    
    
     public function alumnosGrupo($valor ="") {
        $solicitantes = new SolicitudesModel();
        $solicitantes['pau'] = $solicitantes->SELECT("pau.id ,pau.nif, pau.apellido1, pau.apellido2, pau.nombre, pau.email, pau.ciclo, pau.tipo_tasa, pau.deleted_at")
                ->join('ciclos', 'pau.ciclo = ciclos.id', 'LEFT')
                ->where('ciclo', $valor)
                ->findAll();

        echo view('SolicitantesView', $solicitantes);
     }
    
    
    public function borrar($NIA="29292929"){
        $model = new SolicitudesModel();
        $model-> where('nif',$NIF)->delete();
        return redirect()->to('/SolicitudesController');
    }  

 


}
    




   

